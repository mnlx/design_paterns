

// function add(a, b, cb) {
//     let sum = a + b;
//     setTimeout(() => { cb(sum) }, 100);

//     return {
//         onData
//     }
// }

// console.log('f')
// add(199, 2421, (data) => { console.log(data) })
// console.log('l')

// const fs = require('fs');
// const crypto = require('crypto');
// function readJSONThrows(filename, callback) {
//     fs.readFile(filename, 'utf8', (err, data) => {
//         if (err) {
//             return callback(err);
//         }
//         //no errors, propagate just the data
//         callback(null, JSON.parse(data));
//     });
// };

// readJSONThrows('nonJSON.txt', err => console.log(err));


// // try {
// //     readJSONThrows('nonJSON.txt', function (err, result) {
// //         console.log(err);
// //     });
// // } catch (err) {
// //     console.log('This will not catch the JSON parsing exception');
// // }


// process.on('uncaughtException', (err) => {
//     console.error('This will catch at last the ' +
//         'JSON parsing exception: ' + err.message);
//     // Terminates the application with 1 (error) as exit code:
//     // without the following line, the application would continue
//     process.exit(1);
// });


// function loadModule(filename, module, require) {
//     const wrappedSrc = `(function(module, exports, require) {
//     ${fs.readFileSync(filename, 'utf8')}
//     })(module, module.exports, require);`;
//     eval(wrappedSrc);
// }


// const required = (moduleName) => {
//     console.log(`Require invoked for module: ${moduleName}`);
//     const id = require.resolve(moduleName);
//     //[1]
//     if (require.cache[id]) {
//         //[2]
//         return require.cache[id].exports;
//     }
//     //module metadata
//     const module = {
//         exports: {},
//         id: id
//     };
//     //Update the cache
//     console.log(require.cache)
//     require.cache[id] = module;
//     //[3]
//     //[4]
//     //load the module
//     loadModule(id, module, require);
//     //[5]
//     //return exported variables
//     return module.exports;
//     //[6]
// };
// // require.cache = {};
// // require.resolve = (moduleName) => {
// //     /* resolve a full module id from the moduleName */
// // };


// var c = 2;
(new EventEmitter()).on('fileread', file => console.log(file + ' was read'))
    .on('found', (file, match) => console.log('Matched "' + match +
        '" in file ' + file))
    .on('error', err => console.log('Error emitted: ' + err.message));

const EventEmitter = require('events').EventEmitter;
const fs = require('fs');
function findPattern(files, regex) {
    const emitter = new EventEmitter();
    files.forEach(function (file) {
        fs.readFile(file, 'utf8', (err, content) => {
            if (err)
                return emitter.emit('error', err);
            emitter.emit('fileread', file);
            let match;
            if (match = content.match(regex))
                match.forEach(elem => emitter.emit('found', file, elem));
        });
    });
    return emitter;
}


findPattern(
    ['nonJSON.txt'],
    /test/g
)





    // const emitter = new EventEmitter();