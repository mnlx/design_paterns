
const fs = require('fs');
const zlib = require('zlib');
const http = require('http');
const path = require('path');
const file = process.argv[2];
const server = process.argv[3];

console.log(__dirname + '/' + file)
const options = {
    hostname: server,
    port: 3000,
    path: '/',
    method: 'PUT',
    headers: {
        filename: path.basename(__dirname + '/' + file),
        'Content-Type': 'application/octet-stream',
        'Content-Encoding': 'gzip'
    }
}

const req = http.request(options, res => {
    console.log('Server response: ' + res.statusCode);
})

var a = fs.createReadStream(__dirname + '/' + file)
var b = a.pipe(zlib.createGzip())
var c = b.pipe(req)
c.on('finish', () => {
    console.log('File sent!');
}).on('error', () => {
    console.log('File NOT sent!');
})