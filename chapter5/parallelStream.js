const stream = require('stream');

class ParallelStream extends stream.Transform {
    constructor(concurrency, userTranform) {
        super({ objectMode: true });
        this.concurrency = concurrency;
        this.userTranform = userTranform;
        this.running = 0;
        this.terminateCallback = null;
        this.continueCallback = null;
    }

    _transform(chunk, enc, done) {
        this.running++;
        this.userTranform(chunk, enc, this.push.bind(this), this._onComplete.bind(this));
        if (this.running < this.concurrency) {
            done();
        } else {
            this.continueCallback = done;
        }
    }

    _flush(done) {
        if (this.running > 0) {
            this.terminateCallback = done;
        } else {
            done();
        }
    }

    _onComplete(err) {
        this.running--;
        if (err) {
            return this.emit('error', err);
        }
        const tmpCallback = this.continueCallback;
        this.continueCallback = null;
        tmpCallback && tmpCallback();
        if (this.running === 0) {
            this.terminateCallback && this.terminateCallback();
        }
    }
}

const fs = require('fs');
const split = require('split');
const request = require('request');

fs.createReadStream(__dirname + '/' + process.argv[2])
    // .pipe(split())
    .pipe(new ParallelStream(1, (url, enc, push, done) => {
        if (!url) return done();
        request.head(url, (err, response) => {
            push(url + ' is ' + (err ? 'down' : 'up') + '\n');
            done();
        });
    })
    )
    .pipe(fs.createWriteStream('results.txt'))
    .on('finish', () => console.log('All urls have been checked'));

