const zlib = require('zlib');
const crypto = require('crypto');
const combine = require('multipipe');


var compressAndEncrypt = function (password) {
    return combine(
        zlib.createGzip(),
        crypto.createCipher('aes-192-gcm', password)
    )
}

var decryptAndDecompress = function (password) {
    return combine(
        crypto.createCipher('aes-192-gcm', password),
        zlib.createGunzip(),
        fs.createWriteStream('temp_unzip.sql')
    )
}


const fs = require('fs');
const compressAndEncryptStream = compressAndEncrypt;

// fs.createReadStream(__dirname + '/' + process.argv[3])
//     .pipe(compressAndEncryptStream(process.argv[2]))
//     .pipe(fs.createWriteStream(process.argv[3] + '.gz.enc'));


fs.createReadStream(__dirname + '/' + process.argv[3])
    .pipe(decryptAndDecompress(process.argv[2]))
    .on('error', (err) => { console.log('Bottay!') })
