// function DelayedGreeter(name) {
//     this.name = name;
// }

// var great = 'Heehee';

// DelayedGreeter.prototype.greet = function () {
//     setTimeout(function cb() {
//         console.log('Hello ' + this.name, great);
//     }.bind(this).bind(great), 500);
// };
// const greeter = new DelayedGreeter('World');
// greeter.greet(); // will print "Hello undefined"


// class Person {
//     constructor(name, surname, age) {
//         this.name = name;
//         this.surname = surname;
//         this.age = age;
//     }
//     getFullName() {
//         return this.name + ' ' + this.surname;
//     }
//     static older(person1, person2) {
//         return (person1.age >= person2.age) ? person1 : person2;
//     }
// }


// const peeps = new Person('andre', 'duarte', 12)
// const peeps2 = new Person('andre', 'duarte', 12)


// console.log(peeps.getFullName(), Person.older(peeps, peeps2))

// class PersonWithMiddlename extends Person {
//     constructor(name, middlename, surname, age) {
//         super(name, surname, age);
//         this.middlename = middlename;
//     }
//     getFullName() {
//         return this.name + '' + this.middlename + '' + this.surname;
//     }
// }


// const peeps3 = new PersonWithMiddlename('andre', 'pereira', 'duarte', 12)

// console.log(peeps3.getFullName())


// const person = {

//     name: 'Felipe',
//     surname: 'Duarte',

//     get fullname() {
//         return this.name + ' ' + this.surname
//     },

//     set fullname(fullname) {
//         let fullnameSplit = fullname.split(' ');
//         this.name = fullnameSplit[0];
//         // return fullname[0];

//     }
// }

// console.log(person.fullname);
// person.fullname = 'what the heck';
// console.log(person.fullname);


// const profiles = new Map();
// profiles.set('twitter', '@adalovelace');
// profiles.set('facebook', 'adalovelace');
// profiles.set('googleplus', 'ada');
// profiles.set('googlepluse', 'ada', 'test');

// profiles.size; // 3
// profiles.has('twitter'); // true
// profiles.get('twitter'); // "@adalovelace"
// profiles.has('youtube'); // false
// profiles.delete('facebook');
// profiles.has('facebook'); // false
// profiles.get('facebook'); // undefined
// for (const entry of profiles) {
//     console.log(profiles.get(entry[0]));
// }


const s = new Set([0, 1, 2, 2, 3]);
s.add(3); // will not be added
s.size; // 4
s.delete(0);
s.has(0); // false
for (const entry of s) {
    console.log(entry);
}